<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Procesar PHP dentro del mismo formulario</title>
</head>
<body>
    <form action="#" method="POST" >
        <legend>Calculadora</legend>
        <p>Nro1: <input type="text" name="txtNro1"/></p>
        <p>Nro2: <input type="text" name="txtNro2"/></p>
        <p><input type="submit" name="btnSumar" value="Sumar"/> 
        <input type="submit" name="btnRestar" value="Restar"/> 
        <input type="submit" name="btnMutiplicar" value="Multiplicar"/> 
        <input type="submit" name="btnDividir" value="Dividir"/> 
        <input type="submit" name="btnPotencia" value="Potencia"/> 
        <input type="submit" name="btnTangente" value="Tangente"/>
        <input type="submit" name="btnSeno" value="Seno"/> 
        <input type="submit" name="btnPorcentaje" value="Porcentaje"/> 
        <input type="submit" name="btnRaizcuadrada" value="Raiz Cuadrada"/> 
        <input type="submit" name="btnRaizn" value="Raiz n-esima"/> 
        <input type="submit" name="btnInversa" value="Inverso"/> 
        </p>
    </form>    
    <?php 
    if(isset($_POST))
    {
        // Llamar a la clase Calculadora
        include("calculadora.php");        
        $nro1 = $_POST['txtNro1'];
        $nro2 = $_POST['txtNro2'];
        if(isset($_POST['btnSumar']))
        {
            // Instanciar un objeto a traves de la clase
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;
            $calculo->nro2 = $nro2;
            $suma = $calculo->Sumar();
            echo "La suma de los numeros es: " , $suma;
        }
        else  if(isset($_POST['btnRestar']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;   
            $calculo->nro2 = $nro2;            
            $resta = $calculo->Restar();
            echo "La resta es: " , $resta;
        }        
        else  if(isset($_POST['btnDividir']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;   
            $calculo->nro2 = $nro2;            
            $cociente = $calculo->Dividir();
            echo "La division es: " , $cociente;
        }
        else  if(isset($_POST['btnMutiplicar']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;   
            $calculo->nro2 = $nro2;            
            $prod = $calculo->Multiplicar();
            echo "La multi es: " , $prod;
        }  
        else  if(isset($_POST['btnPotencia']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;   
            $calculo->nro2 = $nro2;            
            $pot = $calculo->Potencia();
            echo "La potencia es: " , $pot;
        }
        else  if(isset($_POST['btnTangente']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;             
            $tg = $calculo->Tangente($nro1);
            echo "La tangente es: " , $tg;
        }
        else  if(isset($_POST['btnSeno']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;             
            $sen = $calculo->Seno($nro1);
            echo "El seno es: " , $sen;
        }
        else  if(isset($_POST['btnPorcentaje']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1; 
            $calculo->nro2 = $nro2;
            $porc = $calculo->Porcentaje();
            echo "El porcentaje del numero es: " , $porc;
        }
        else  if(isset($_POST['btnRaizcuadrada']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1; 
            $rc = $calculo->RaizCuadrada($nro1);
            echo "La raiz cuadrada es: " , $rc;
        }
        else  if(isset($_POST['btnRaizn']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1; 
            $calculo->nro2 = $nro2;
            $rn = $calculo->RaizN();
            echo "La raiz es: " , $rn;
        }
        else  if(isset($_POST['btnInversa']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1; 
            $calculo->nro2 = $nro2;
            $in = $calculo->Inversa($nro1);
            echo "La inversa es: " , $in;
        }
    }
    ?>
</body>
</html>